FROM ubuntu:latest
RUN apt-get update -y
RUN apt-get install -y python3-pip python3-dev python3-setuptools build-essential
ADD requirements.txt .
RUN pip3 install -r requirements.txt
ADD . .
EXPOSE 8080
CMD ["python3","backend/main.py"]
