# Connect With Nature - 11 AM Group 9

![alt text](/public/leaf.png/?raw=true)  

The goal of this project is to promote civic appreciation of nature and raise awareness for future conservation efforts by encouraging visitation to the national parks. We hope that this site will you help you better appreciate the importance of preservation and consider becoming involved in current conservation efforts!

## Installation & Dependencies

After cloning the repository, run `npm install` to install required project dependencies.<br>
Our project stored private keys in /src/Pages/config.js for use in /src/Pages/StatTable.js.<br>
The config.js file looks as follows:<br>
~~~~
const apiKey = 'GITLAB_API_KEY';
const projID = 'PROJECT_ID';

export { apiKey, projID };
~~~~
To run the project in development mode, run `npm start` and navigate to [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Deployment

On the AWS CLI, first configure personal project credentials including Access Key ID and Secret Access Key.<br>
Within the project directory, run `npm run build` and `npm run deploy` for the site to go live.

## Project Estimations

Estimated Completion Time (Phase I): 20 hrs<br>
Actual Completion Time (Phase I): 30 hrs

## Links
[Production Website](https://www.connectwithnature.me) <br>
[Postman](https://documenter.getpostman.com/view/6751555/S11LsHR5)
