from sqlalchemy.dialects import postgresql
from sqlalchemy import Column, Integer, VARCHAR
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine
import os

Base = declarative_base()

class ParkInstance(Base):

    __tablename__ = 'parks'

    name = Column(VARCHAR(length=250), primary_key = True)
    states = Column(VARCHAR(length=250))
    summary = Column(VARCHAR(length=5000))
    directions = Column(VARCHAR(length=250))
    weather = Column(VARCHAR(length=250))
    phone = Column(VARCHAR(length=250))
    email = Column(VARCHAR(length=250))
    campgrounds = Column(Integer)
    fee = Column(Integer)
    pics = Column(postgresql.ARRAY(VARCHAR(length=250)))
    events = Column(postgresql.ARRAY(VARCHAR(length=250)))
    orgs = Column(postgresql.ARRAY(VARCHAR(length=250)))

class StateInstance(Base):

    __tablename__ = 'states'

    name = Column(VARCHAR(length=250), primary_key = True)
    nickname = Column(VARCHAR(length=250))
    summary = Column(VARCHAR(length=5000))
    capital = Column(VARCHAR(length=250))
    areaRank = Column(VARCHAR(length=250))
    densityRank = Column(VARCHAR(length=250))
    popRank = Column(VARCHAR(length=250))
    parks = Column(postgresql.ARRAY(VARCHAR(length=250)))
    pic = Column(postgresql.ARRAY(VARCHAR(length=250)))
    events = Column(postgresql.ARRAY(VARCHAR(length=250)))
    area = Column(VARCHAR(length=250))
    population = Column(VARCHAR(length=250))
    incomeRank = Column(VARCHAR(length=250))
    orgs = Column(postgresql.ARRAY(VARCHAR(length=250)))

class EventInstance(Base):

    __tablename__ = 'events'

    name = Column(VARCHAR(length=250), primary_key = True)
    date = Column(postgresql.ARRAY(VARCHAR(length=250)))
    location = Column(VARCHAR(length=5000))
    park = Column(VARCHAR(length=250))
    fee = Column(VARCHAR(length=250))
    time = Column(VARCHAR(length=250))
    summary = Column(VARCHAR(length=5000))
    pics = Column(postgresql.ARRAY(VARCHAR(length=250)))
    states = Column(VARCHAR(length=250))
    orgs = Column(postgresql.ARRAY(VARCHAR(length=250)))

class OrgInstance(Base):

    __tablename__ = 'orgs'

    name = Column(VARCHAR(length=250), primary_key = True)
    tagline = Column(VARCHAR(length=5000))
    mission = Column(VARCHAR(length=5000))
    state = Column(VARCHAR(length=250))
    category = Column(VARCHAR(length=250))
    rating = Column(VARCHAR(length=250))
    pics = Column(VARCHAR())
    parks = Column(postgresql.ARRAY(VARCHAR(length=250)))
    events = Column(postgresql.ARRAY(VARCHAR(length=250)))

def get_env_variable(name):
    try:
        print(os.environ[name])
        return os.environ[name]
    except KeyError:
        message = "Expected environment variable '{}' not set.".format(name)
        raise Exception(message)

#POSTGRES_URL = get_env_variable("URL")
#POSTGRES_USER = get_env_variable("USER")
#POSTGRES_PW = get_env_variable("PW")
#POSTGRES_DB = get_env_variable("DB")

POSTGRES_URL="connectwithnature1.cz5vj76nkxrn.us-east-2.rds.amazonaws.com"
POSTGRES_USER="postgres"
POSTGRES_PW="PatrickStar*"
POSTGRES_DB="connectwithnature"

DB_URL = 'postgresql+psycopg2://{user}:{pw}@{url}/{db}'.format(user=POSTGRES_USER,pw=POSTGRES_PW,url=POSTGRES_URL,db=POSTGRES_DB)
SQLALCHEMY_DATABASE_URI = os.getenv('SQLALCHEMY_DATABASE_URI', DB_URL)
engine = create_engine(SQLALCHEMY_DATABASE_URI)
