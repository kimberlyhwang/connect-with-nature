import os
from unittest import main, TestCase
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from schema import Base, engine, ParkInstance, StateInstance, EventInstance, OrgInstance, get_env_variable
from main import dict_creator
Base.metadata.bind = engine

DBSession = sessionmaker(bind=engine)
session = DBSession()

class UnitTests (TestCase) :
    def test_source_insert_1(self):
        s = OrgInstance(
            name  = "Lorem ipsum",
            tagline = "dolor sit amet",
            mission = "consectetur adipiscing elit",
            state = "Morbi interdum facilisis",
            category = "dui posuere placerat",
            rating = "Aenean scelerisque libero",
            pics = "eget dui ultricies",
            parks = "ac tempus sapien mollis",
            events = "Nam ornare viverra"
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(OrgInstance).filter_by(name = "Lorem ipsum").one()
            self.assertEqual(str(r.tagline), 'dolor sit amet')
            session.query(OrgInstance).filter_by(name = "Lorem ipsum").delete()
            session.commit()
        except:
            session.rollback()
            
    def test_source_insert_2(self):
        s = ParkInstance(
            name  = "Lorem ipsum",
            states = "dolor sit amet",
            summary = "consectetur adipiscing elit",
            directions = "Morbi interdum facilisis",
            weather = "dui posuere placerat",
            phone = "Aenean scelerisque libero",
            email = "eget dui ultricies",
            campgrounds = 65,
            fee = 34,
            pics = "Vivamus pharetra malesuada",
            events = "Fusce nec mauris",
            orgs = "Quisque sit amet"
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(ParkInstance).filter_by(states = "dolor sit amet").one()
            self.assertEqual(str(r.pics), 'Vivamus pharetra malesuada')
            session.query(ParkInstance).filter_by(states = "dolor sit amet").delete()
            session.commit()
        except:
            session.rollback()
            
    def test_source_insert_3(self):
        s = StateInstance(
            name  = "Lorem ipsum",
            nickname = "dolor sit amet",
            summary = "consectetur adipiscing elit",
            capital = "Morbi interdum facilisis",
            areaRank = "dui posuere placerat",
            densityRank = "Aenean scelerisque libero",
            popRank = "eget dui ultricies",
            parks = "et ultrices posuere",
            pic = "ultricies vehicula felis",
            events = "Vivamus pharetra malesuada",
            area = "Fusce nec mauris",
            population = "Quisque sit amet",
            incomeRank = "Mauris sit amet",
            orgs = "bibendum consequat"   
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(StateInstance).filter_by(population = "Quisque sit amet").one()
            self.assertEqual(str(r.parks), "et ultrices posuere")
            session.query(StateInstance).filter_by(population = "Quisque sit amet").delete()
            session.commit()
        except:
            session.rollback()
            
    def test_source_insert_4(self):
        s = EventInstance(
            name  = "Lorem ipsum",
            date = "dolor sit amet",
            location = "consectetur adipiscing elit",
            park = "Morbi interdum facilisis",
            fee = "dui posuere placerat",
            time = "Aenean scelerisque libero",
            summary = "eget dui ultricies",
            pics = "et ultrices posuere",
            states = "ultricies vehicula felis",
            orgs = "Vivamus pharetra malesuada"
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(EventInstance).filter_by(location = "consectetur adipiscing elit").one()
            self.assertEqual(str(r.orgs), "Vivamus pharetra malesuada")
            session.query(EventInstance).filter_by(location = "consectetur adipiscing elit").delete()
            session.commit()
        except:
            session.rollback()
      
    def test_source_insert_5(self):
        s = OrgInstance(
            name  = "Lorem ipsum",
            tagline = "dolor sit amet",
            mission = "consectetur adipiscing elit",
            state = "Morbi interdum facilisis",
            category = "dui posuere placerat",
            rating = "Aenean scelerisque libero",
            pics = "eget dui ultricies",
            parks = "ac tempus sapien mollis",
            events = "Nam ornare viverra"
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(OrgInstance).filter_by(tagline = "dolor sit amet").one()
            self.assertEqual(str(r.parks), 'ac tempus sapien mollis')
            session.query(OrgInstance).filter_by(tagline = "dolor sit amet").delete()
            session.commit()
        except:
            session.rollback()
            
    def test_source_insert_6(self):
        s = ParkInstance(
            name  = "Lorem ipsum",
            states = "dolor sit amet",
            summary = "consectetur adipiscing elit",
            directions = "Morbi interdum facilisis",
            weather = "dui posuere placerat",
            phone = "Aenean scelerisque libero",
            email = "eget dui ultricies",
            campgrounds = 65,
            fee = 34,
            pics = "Vivamus pharetra malesuada",
            events = "Fusce nec mauris",
            orgs = "Quisque sit amet"
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(ParkInstance).filter_by(fee = 34).one()
            self.assertEqual(str(r.campgrounds), 65)
            session.query(ParkInstance).filter_by(fee = 34).delete()
            session.commit()
        except:
            session.rollback()
            
    def test_source_insert_7(self):
        s = StateInstance(
            name  = "Lorem ipsum",
            nickname = "dolor sit amet",
            summary = "consectetur adipiscing elit",
            capital = "Morbi interdum facilisis",
            areaRank = "dui posuere placerat",
            densityRank = "Aenean scelerisque libero",
            popRank = "eget dui ultricies",
            parks = "et ultrices posuere",
            pic = "ultricies vehicula felis",
            events = "Vivamus pharetra malesuada",
            area = "Fusce nec mauris",
            population = "Quisque sit amet",
            incomeRank = "Mauris sit amet",
            orgs = "bibendum consequat"   
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(StateInstance).filter_by(densityRank = "Aenean scelerisque libero").one()
            self.assertEqual(str(r.orgs), "bibendum consequat")
            session.query(StateInstance).filter_by(densityRank = "Aenean scelerisque libero").delete()
            session.commit()
        except:
            session.rollback()
            
    def test_source_insert_8(self):
        s = EventInstance(
            name  = "Lorem ipsum",
            date = "dolor sit amet",
            location = "consectetur adipiscing elit",
            park = "Morbi interdum facilisis",
            fee = "dui posuere placerat",
            time = "Aenean scelerisque libero",
            summary = "eget dui ultricies",
            pics = "et ultrices posuere",
            states = "ultricies vehicula felis",
            orgs = "Vivamus pharetra malesuada"
        )

        try:
            session.add(s)
            session.commit()
            r = session.query(EventInstance).filter_by(time = "Aenean scelerisque libero").one()
            self.assertEqual(str(r.time), "Aenean scelerisque libero")
            session.query(EventInstance).filter_by(time = "Aenean scelerisque libero").delete()
            session.commit()
        except:
            session.rollback()
    
    def test_dict_creator(self):
        s1 = StateInstance(
            name  = "Lorem ipsum",
            nickname = "dolor sit amet",
            summary = "consectetur adipiscing elit",
            capital = "Morbi interdum facilisis",
            areaRank = "dui posuere placerat",
            densityRank = "Aenean scelerisque libero",
            popRank = "eget dui ultricies",
            parks = "et ultrices posuere",
            pic = "ultricies vehicula felis",
            events = "Vivamus pharetra malesuada",
            area = "Fusce nec mauris",
            population = "Quisque sit amet",
            incomeRank = "Mauris sit amet",
            orgs = "bibendum consequat"   
        )
        s2 = StateInstance(
            name  = "Lorem ipsum",
            nickname = "dolor sit amet",
            summary = "consectetur adipiscing elit",
            capital = "Morbi interdum facilisis",
            areaRank = "dui posuere placerat",
            densityRank = "Aenean scelerisque libero",
            popRank = "eget dui ultricies",
            parks = "et ultrices posuere",
            pic = "ultricies vehicula felis",
            events = "Vivamus pharetra malesuada",
            area = "Fusce nec mauris",
            population = "Quisque sit amet",
            incomeRank = "Mauris sit amet",
            orgs = "bibendum consequat"   
        )
        l = [s1, s2]
        dc = dict_creator(l)
        assert type(dc) is list
        assert type(dc[0]) is dict
        
    def test_get_env_variable(self):
        try:
            get_env_variable("siweilovesnatures")
        except Exception:
            assert(True)
    

if __name__ == "__main__" :
    main()
