#!/bin/sh

import logging
import os
import sys

from flask import Flask, request, json

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from flask_cors import CORS

from schema import engine, Base, ParkInstance, StateInstance, EventInstance, OrgInstance

app = Flask(__name__)

CORS(app)

Base.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
session = DBSession()


@app.route("/park", methods=["GET"])
def index():
    park_name = request.args.get("park_name", None)
    if park_name == None:
        try:
            park_all = session.query(ParkInstance).all()
        except:
            session.rollback()
        park_dict = dict_creator(park_all)
        return json_response(park_dict)
    else:
        try:
            park_single = session.query(ParkInstance).filter_by(name=park_name)
        except:
            session.rollback()
        park_single_dict = dict_creator(park_single)
        return json_response(park_single_dict)


def dict_creator(instanceList):
    ret = []
    for obj in instanceList:
        objDict = {}
        d = obj.__dict__
        for k in d:
            if k == "_sa_instance_state":
                continue
            else:
                objDict[k] = d[k]
        ret.append(objDict)
    return ret


@app.route("/state", methods=["GET"])
def index2():
    state_name = request.args.get("state_name", None)
    if state_name == None:
        try:
            state_all = session.query(StateInstance).all()
        except:
            session.rollback()
        state_dict = dict_creator(state_all)
        return json_response(state_dict)
    else:
        try:
            state_single = session.query(StateInstance).filter_by(name=state_name)
        except:
            session.rollback()
        state_single_dict = dict_creator(state_single)
        return json_response(state_single_dict)


@app.route("/event", methods=["GET"])
def index3():
    event_name = request.args.get("event_name", None)
    if event_name == None:
        try:
            event_all = session.query(EventInstance).all()
        except:
            session.rollback()
        event_dict = dict_creator(event_all)
        return json_response(event_dict)
    else:
        try:
            event_single = session.query(EventInstance).filter_by(name=event_name)
        except:
            session.rollback()
        event_single_dict = dict_creator(event_single)
        return json_response(event_single_dict)


@app.route("/org", methods=["GET"])
def index4():
    org_name = request.args.get("org_name", None)
    if org_name == None:
        try:
            org_all = session.query(OrgInstance).all()
        except:
            session.rollback()
        org_dict = dict_creator(org_all)
        return json_response(org_dict)
    else:
        try:
            org_single = session.query(OrgInstance).filter_by(name=org_name)
        except:
            session.rollback()
        org_single_dict = dict_creator(org_single)
        return json_response(org_single_dict)


def json_response(payload, status=200):
    return (
        json.dumps(payload),
        status,
        {"content-type": "application/json", "Access-Control-Allow-Origin": "*"},
    )


@app.errorhandler(500)
def server_error(e):
    logging.exception("An error occurred during a request.")
    return (
        """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(
            e
        ),
        500,
    )


if __name__ == "__main__":
    print("Starting")
    app.run("0.0.0.0", port=8080)
