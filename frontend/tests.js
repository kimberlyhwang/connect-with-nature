import React from 'react';
import { configure, shallow } from 'enzyme';
import { expect } from 'chai';
import Adapter from 'enzyme-adapter-react-16';
configure({ adapter: new Adapter() });

import AboutCard from './Components/AboutCard';
import ButtonAppBar from './Components/ButtonAppBar';
import ListInfoCard from './Components/ListInfoCard';
import ParkInfoCard from './Components/ParkInfoCard';
import RelatedCard from './Components/RelatedCard';

describe('tests for <AboutCard>', () => {
  it('can set text', () => {
    const container = shallow(<AboutCard title="Project Goal" text="Lorem Ipsum"/>);
    expect(container.prop('text')).equal("Lorem Ipsum");
  });
  
  it('can set links', () => {
    const placeholderLinks = ["linkOne", "linkTwo", "linkThree"];
    const container = shallow(<AboutCard links={placeholderLinks} />);
    expect(container.prop('links')).lengthOf(placeholderLinks.length);
  });
  it('can create with props', () => {
    const container = shallow(<AboutCard />);
    expect(Object.keys(container.prop('classes'))).lengthOf(5);
  });
});

describe('tests for <ButtonAppBar>', () => {
  it('can create with props', () => {
    const container = shallow(<ButtonAppBar />);
    expect(Object.keys(container.prop('classes'))).lengthOf(2);
  });
});

describe('tests for <ListInfoCard>', () => {
  it('can create with props', () => {
    const container = shallow(<ListInfoCard />);
    expect(Object.keys(container.prop('classes'))).lengthOf(3);
  });
  it('can set title', () => {
    const container = shallow(<ListInfoCard title='test title'/>);
    expect(container.prop('title')).equal('test title');
  });
});

describe('tests for <ParkInfoCard>', () => {
  it('can create with props', () => {
    const container = shallow(<ParkInfoCard />);
    expect(Object.keys(container.prop('classes'))).lengthOf(3);
  });
  it('can set summary', () => {
    const container = shallow(<ParkInfoCard summary='test summary'/>);
    expect(container.prop('summary')).equal('test summary');
  });
});

describe('tests for <RelatedCard>', () => {
  it('can create with props', () => {
    const container = shallow(<RelatedCard />);
    expect(Object.keys(container.prop('classes'))).lengthOf(3);
  });
  it('can set text', () => {
    const container = shallow(<RelatedCard helo='test text'/>);
    expect(container.prop('helo')).equal('test text');
  });
});