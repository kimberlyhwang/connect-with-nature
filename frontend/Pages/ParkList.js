import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListInfoCard from "../Components/ListInfoCard.js";
import Pagination from "../Components/Pagination.js";
import axios from "axios";

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: "inherit",
    width: "inherit"
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  container: {
    backgroundColor: "#ebfaeb",
    paddingTop: "3vh",
    minHeight: "100vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  pagination: {}
});

function FormRow(props) {
  const data = props;
  if (data === undefined || data.data.length === 0) {
    return (
      <React.Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Parks/1"
              coverName="Yellowstone National Park"
              coverDescript="Idaho, Montana, Wyoming"
              coverImage="https://www.nps.gov/common/uploads/structured_data/3C7D368B-1DD8-B71B-0B92925065B93463.jpg"
              attribute1="States"
              attribute1val="ID, MT, WY"
              attribute2="Phone Number"
              attribute2val="3073447381"
              attribute3="Email Address"
              attribute3val="yell_visitor_services@nps.gov"
              attribute4="Number of Campgrounds"
              attribute4val="13"
              attribute5="Entrance Fees"
              attribute5val="35"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Parks/1"
              coverName="Yosemite National Park"
              coverDescript="California"
              coverImage="https://www.nps.gov/common/uploads/structured_data/3C84C6CF-1DD8-B71B-0B1C7CB883AA8FB1.jpg"
              attribute1="States"
              attribute1val="CA"
              attribute2="Phone Number"
              attribute2val="2093720200"
              attribute3="Email Address"
              attribute3val="yose_web_manager@nps.gov"
              attribute4="Number of Campgrounds"
              attribute4val="9"
              attribute5="Entrance Fees"
              attribute5val="35"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Parks/1"
              coverName="Grand Canyon National Park"
              coverDescript="Arizona"
              coverImage="https://www.nps.gov/common/uploads/structured_data/3C7B143E-1DD8-B71B-0BD4A1EF96847292.jpg"
              attribute1="States"
              attribute1val="AZ"
              attribute2="Phone Number"
              attribute2val="9286387888"
              attribute3="Email Address"
              attribute3val="grca_information@nps.gov"
              attribute4="Number of Campsites"
              attribute4val="3"
              attribute5="Entrance Fees"
              attribute5val="35"
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  } else {
    const park1 = data.data[0];
    const park2 = data.data[1];
    const park3 = data.data[2];
    const park4 = data.data[3];
    const park5 = data.data[4];
    return (
      <React.Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"Parks/" + park1.name}
              coverName={park1.name}
              coverDescript="potate"
              coverImage={park1.pics[0]}
              attribute1="States"
              attribute1val={park1.states}
              attribute2="Phone Number"
              attribute2val={park1.phone}
              attribute3="Email Address"
              attribute3val={park1.email}
              attribute4="Number of Campsites"
              attribute4val={park1.campgrounds}
              attribute5="Entrance Fees"
              attribute5val={park1.fee}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"Parks/" + park2.name}
              coverName={park2.name}
              coverDescript="potate"
              coverImage={park2.pics[0]}
              attribute1="States"
              attribute1val={park2.states}
              attribute2="Phone Number"
              attribute2val={park2.phone}
              attribute3="Email Address"
              attribute3val={park2.email}
              attribute4="Number of Campsites"
              attribute4val={park2.campgrounds}
              attribute5="Entrance Fees"
              attribute5val={park2.fee}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"Parks/" + park3.name}
              coverName={park3.name}
              coverDescript="potate"
              coverImage={park3.pics[0]}
              attribute1="States"
              attribute1val={park3.states}
              attribute2="Phone Number"
              attribute2val={park3.phone}
              attribute3="Email Address"
              attribute3val={park3.email}
              attribute4="Number of Campsites"
              attribute4val={park3.campgrounds}
              attribute5="Entrance Fees"
              attribute5val={park3.fee}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"Parks/" + park4.name}
              coverName={park4.name}
              coverDescript="potate"
              coverImage={park4.pics[0]}
              attribute1="States"
              attribute1val={park4.states}
              attribute2="Phone Number"
              attribute2val={park4.phone}
              attribute3="Email Address"
              attribute3val={park4.email}
              attribute4="Number of Campsites"
              attribute4val={park4.campgrounds}
              attribute5="Entrance Fees"
              attribute5val={park4.fee}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"Parks/" + park5.name}
              coverName={park5.name}
              coverDescript="potate"
              coverImage={park5.pics[0]}
              attribute1="States"
              attribute1val={park5.states}
              attribute2="Phone Number"
              attribute2val={park5.phone}
              attribute3="Email Address"
              attribute3val={park5.email}
              attribute4="Number of Campsites"
              attribute4val={park5.campgrounds}
              attribute5="Entrance Fees"
              attribute5val={park5.fee}
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

class ParkList extends Component {
  state = {
    allParks: [],
    currentParks: [],
    currentPage: null,
    totalPages: null
  };

  scrapeProjectInfo() {
    axios.all([axios.get("https://api.connectwithnature.me/park")]).then(
      axios.spread(parkResponse => {
        this.setState({
          allParks: parkResponse.data
        });
      })
    );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  onPageChanged = data => {
    const { allParks } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentParks = allParks.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentParks, totalPages });
  };

  render() {
    const { classes } = this.props;
    const { allParks, currentParks } = this.state;
    const totalParks = allParks.length;
    return (
      <div className={classes.container}>
        <Grid container className={classes.root} spacing={8}>
          <Grid container item xs={12} spacing={16}>
            <FormRow data={currentParks} />
          </Grid>
        </Grid>

        {totalParks !== 0 && (
          <Pagination
            className={classes.pagination}
            totalRecords={50}
            pageLimit={5}
            onPageChanged={this.onPageChanged}
          />
        )}
      </div>
    );
  }
}

ParkList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(ParkList);
