import React, { Component } from "react";
import StateInfoCard from "../Components/StateInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";

import axios from "axios";

const Header = {
  color: "black",
  fontSize: "50px",
  textAlign: "center"
};
const Background = {
  backgroundColor: "#ebfaeb",
  minHeight: "100vh",
  height: "auto"
};

const Container = {
  display: "flex",
  flexGrow: 1,
  flexDirection: "row",
  justifyContent: "space-evenly"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  flexWrap: "wrap"
};

const ContainerChild = {
  display: "flex",
  flexDirection: "row",
  maxHeight: "300px",
  marginRight: "80px",
  marginLeft: "80px"
};

class StatesInstance extends Component {
  state = {
    data: {}
  };

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(
          `https://d3aeapuik0gur3.cloudfront.net/state?state_name=${
            this.props.match.params.name
          }`
        )
      ])
      .then(
        axios.spread(stateResponse => {
          this.setState({
            data: stateResponse.data[0]
          });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  render() {
    const { data } = this.state;
    const { name, summary, parks, pic, events, orgs } =
      data !== undefined ? data : "";
    var parkStr = parks;
    var parkName;
    if (parkStr) {
      parkName = parkStr[0];
    }
    var orgsArr = orgs;
    var orgName;
    if (orgsArr) {
      orgName = orgsArr[0];
    }
    var eventsArr = events;
    var eventsName;
    if (eventsArr) {
      eventsName = eventsArr[0];
    }
    return (
      <React.Fragment>
        <div style={Background}>
          <div>
            <br />
            <h1 style={Header}>{name}</h1>
            <br />
          </div>
          <div style={Container}>
            <img alt="" style={ContainerChild} src={pic} />
          </div>
          <br />
          <StateInfoCard style={ContainerChild} summary={summary} />
          <br />
          <div style={RelatedContainer}>
            {parkName !== undefined && parkName.length > 0 && (
              <RelatedCard
                title="Park"
                link={"/Parks/" + parkName}
                text={parkName}
              />
            )}
            {events && (
              <RelatedCard
                title="Events"
                link={"/Events/" + eventsName}
                text={eventsName}
              />
            )}
            {orgs && (
              <RelatedCard
                title="Organization"
                link={"/Orgs/" + orgName}
                text={orgName}
              />
            )}
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default StatesInstance;
