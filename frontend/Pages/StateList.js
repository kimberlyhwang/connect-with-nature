import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListInfoCard from "../Components/ListInfoCard.js";
import Pagination from "../Components/Pagination.js";
import axios from "axios";

//pass the paginated data into form row which will display the three cards at a time

function FormRow(props) {
  const data = props;
  if (data === undefined || data.data.length === 0) {
    return (
      <React.Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="States/1"
              coverName="Idaho"
              coverDescript="Potatoes"
              coverImage="https://upload.wikimedia.org/wikipedia/commons/a/a4/Flag_of_Idaho.svg"
              attribute1="Capital"
              attribute1val="Boise"
              attribute2="Nickname"
              attribute2val="Gem State"
              attribute3="Slogan"
              attribute3val="Great Potatoes. Tasty Destinations."
              attribute4="Area Rank"
              attribute4val="14"
              attribute5="Density Rank"
              attribute5val="44"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="States/2"
              coverName="California"
              coverDescript="Hipsters"
              coverImage="https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg"
              attribute1="Capital"
              attribute1val="Sacramento"
              attribute2="Nickname"
              attribute2val="The Golden State"
              attribute3="Slogan"
              attribute3val="No slogan."
              attribute4="Area Rank"
              attribute4val="3"
              attribute5="Density Rank"
              attribute5val="11"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="States/3"
              coverName="Arizona"
              coverDescript="Flyover State"
              coverImage="https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arizona.svg"
              attribute1="Capital"
              attribute1val="Phoenix"
              attribute2="Nickname"
              attribute2val="The Grand Canyon State"
              attribute3="Slogan"
              attribute3val="The Grand Canyon State"
              attribute4="Area Rank"
              attribute4val="6"
              attribute5="Density Rank"
              attribute5val="33"
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  } else {
    const state1 = data.data[0];
    const state2 = data.data[1];
    const state3 = data.data[2];
    const state4 = data.data[3];
    const state5 = data.data[4];
    return (
      <React.Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"States/" + state1.name}
              coverName={state1.name}
              coverDescript="potate"
              coverImage={state1.pic[0]}
              attribute1="Capital"
              attribute1val={state1.capital}
              attribute2="Nickname"
              attribute2val={state1.nickname}
              attribute3="Area Rank"
              attribute3val={state1.areaRank}
              attribute4="Density Rank"
              attribute4val={state1.densityRank}
              attribute5="Population Rank"
              attribute5val={state1.popRank}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"States/" + state2.name}
              coverName={state2.name}
              coverDescript="potate"
              coverImage={state2.pic[0]}
              attribute1="Capital"
              attribute1val={state2.capital}
              attribute2="Nickname"
              attribute2val={state2.nickname}
              attribute3="Area Rank"
              attribute3val={state2.areaRank}
              attribute4="Density Rank"
              attribute4val={state2.densityRank}
              attribute5="Population Rank"
              attribute5val={state2.popRank}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"States/" + state3.name}
              coverName={state3.name}
              coverDescript="potate"
              coverImage={state3.pic[0]}
              attribute1="Capital"
              attribute1val={state3.capital}
              attribute2="Nickname"
              attribute2val={state3.nickname}
              attribute3="Area Rank"
              attribute3val={state3.areaRank}
              attribute4="Density Rank"
              attribute4val={state3.densityRank}
              attribute5="Population Rank"
              attribute5val={state3.popRank}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"States/" + state4.name}
              coverName={state4.name}
              coverDescript="potate"
              coverImage={state4.pic[0]}
              attribute1="Capital"
              attribute1val={state4.capital}
              attribute2="Nickname"
              attribute2val={state4.nickname}
              attribute3="Area Rank"
              attribute3val={state4.areaRank}
              attribute4="Density Rank"
              attribute4val={state4.densityRank}
              attribute5="Population Rank"
              attribute5val={state4.popRank}
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link={"States/" + state5.name}
              coverName={state5.name}
              coverDescript="potate"
              coverImage={state5.pic[0]}
              attribute1="Capital"
              attribute1val={state5.capital}
              attribute2="Nickname"
              attribute2val={state5.nickname}
              attribute3="Area Rank"
              attribute3val={state5.areaRank}
              attribute4="Density Rank"
              attribute4val={state5.densityRank}
              attribute5="Population Rank"
              attribute5val={state5.popRank}
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  }
}

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: "inherit",
    width: "inherit"
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  container: {
    backgroundColor: "#ebfaeb",
    paddingTop: "3vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  pagination: {}
});

class StateList extends Component {
  state = {
    allStates: [],
    currentStates: [],
    currentPage: null,
    totalPages: null
  };

  scrapeProjectInfo() {
    axios.all([axios.get("https://d3aeapuik0gur3.cloudfront.net/state")]).then(
      axios.spread(stateResponse => {
        this.setState({
          allStates: stateResponse.data
        });
      })
    );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  onPageChanged = data => {
    const { allStates } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentStates = allStates.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentStates, totalPages });
  };

  render() {
    const { classes } = this.props;
    // const { allStates, currentStates, currentPage, totalPages } = this.state;
    const { allStates, currentStates } = this.state;
    const totalStates = allStates.length;
    return (
      <div className={classes.container}>
        <Grid container className={classes.root} spacing={8}>
          <Grid container item xs={12} spacing={16}>
            <FormRow data={currentStates} />
          </Grid>
        </Grid>

        {totalStates !== 0 && (
          <Pagination
            className={classes.pagination}
            totalRecords={50}
            pageLimit={5}
            onPageChanged={this.onPageChanged}
          />
        )}
      </div>
    );
  }
}

StateList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(StateList);
