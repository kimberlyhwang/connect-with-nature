import React, { Component } from "react";
import StateInfoCard from "../Components/StateInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";

const Header = {
  color: "black",
  fontSize: "50px",
  textAlign: "center"
};
const Background = {
  backgroundColor: "#ebfaeb"
};

const Container = {
  display: "flex",
  flexGrow: 1,
  flexDirection: "row",
  justifyContent: "space-evenly"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  flexWrap: "wrap"
};

const ContainerChild = {
  display: "flex",
  flexDirection: "row",
  maxHeight: "300px",
  marginRight: "80px",
  marginLeft: "80px"
};

// const InfoContainer = {
//   display: "flex",
//   flexGrow: 1
// }

class StatesInstance extends Component {
  render() {
    return (
      <React.Fragment>
        <div style={Background}>
          <div>
            <br />
            <h1 style={Header}>Idaho</h1>
            <br />
          </div>
          <div style={Container}>
            <img
              alt=""
              style={ContainerChild}
              src={
                "https://upload.wikimedia.org/wikipedia/commons/a/a4/Flag_of_Idaho.svg"
              }
            />
          </div>
          <br />
          <StateInfoCard
            style={ContainerChild}
            summary={
              "Idaho is a state in the northwestern region of the United States. It borders the state of Montana to the east and northeast, Wyoming to the east, Nevada and Utah to the south, and Washington and Oregon to the west. To the north, it shares a small portion of the Canadian border with the province of British Columbia. With a population of approximately 1.7 million and an area of 83,569 square miles (216,440 km2), Idaho is the 14th largest, the 12th least populous and the 7th least densely populated of the 50 U.S. states. The state's capital and largest city is Boise."
            }
          />
          <br />
          <div style={RelatedContainer}>
            <RelatedCard
              title="Parks"
              link="/Parks/1"
              text="Yellowstone National Park"
            />
            <RelatedCard
              title="Events"
              link="/Events/2"
              text="Mammoth: Snowshoe Discovery"
            />
            <RelatedCard
              title="Organizations"
              link="/Orgs/1"
              text="Wildlife Conservation Network"
            />
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default StatesInstance;
