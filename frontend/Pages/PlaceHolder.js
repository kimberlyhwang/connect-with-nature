import React, { Component } from "react";
import patrick from "../Components/hmm.gif"

class PlaceHolder extends Component {
  render() {
    return (
      <React.Fragment>
        <div>
          <img src={patrick} alt="thinking..." />
        </div>

      </React.Fragment>
    );
  }
}

export default PlaceHolder;
