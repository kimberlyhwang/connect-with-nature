import React, { Component } from "react";
import ParkInfoCard from "../Components/ParkInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";
import axios from "axios";
import patrick from "../Components/hmm.gif";

const Header = {
  color: "black",
  fontSize: "50px",
  textAlign: "center"
};
const Background = {
  backgroundColor: "#ebfaeb",
  minHeight: "100vh",
  height: "auto"
};

const Container = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  flexWrap: "wrap"
};

class ParksInstance extends Component {
  state = {
    data: {
      pics: [patrick]
    }
  };

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(
          `https://d3aeapuik0gur3.cloudfront.net/park?park_name=${
            this.props.match.params.name
          }`
        )
      ])
      .then(
        axios.spread(parksResponse => {
          this.setState({
            data: parksResponse.data[0]
          });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  render() {
    var jsonStates = require("../data/states.json");
    const { data } = this.state;
    const { name, pics, states, summary, directions, weather, events, orgs } =
      data !== undefined ? data : "";
    var orgsArr = orgs;
    var orgName;
    if (orgsArr) {
      orgName = orgsArr[0];
    }
    var eventsArr = events;
    var eventsName;
    if (eventsArr) {
      eventsName = eventsArr[0];
    }
    var str = states;
    var res;
    if (states) {
      res = str.split(",");
      str = res[0];
    }
    return (
      <React.Fragment>
        <div style={Background}>
          <div>
            <br />
            <h1 style={Header}>{name}</h1>
            <br />
          </div>
          <div style={Container}>
            <img alt="" className="d-block h-50 w-50" src={pics[0]} />
          </div>
          <div>
            <br />
            <ParkInfoCard
              states={states}
              summary={summary}
              directions={directions}
              weather={weather}
            />
            <br />
          </div>
          <div style={RelatedContainer}>
            {states !== undefined && states.length > 0 && (
              <RelatedCard
                title="States"
                link={"/States/" + jsonStates[str]}
                text={jsonStates[str]}
              />
            )}
            {events && (
              <RelatedCard
                title="Events"
                link={"/Events/" + eventsName}
                text={eventsName}
              />
            )}
            {orgs && (
              <RelatedCard
                title="Organization"
                link={"/Orgs/" + orgName}
                text={orgName}
              />
            )}
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default ParksInstance;
