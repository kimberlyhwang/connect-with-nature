import React, { Component } from "react";
import OrgInfoCard from "../Components/OrgInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";
import axios from "axios";

const Header = {
  color: "black",
  fontSize: "50px",
  textAlign: "center"
};
const Background = {
  backgroundColor: "#ebfaeb",
  minHeight: "100vh",
  height: "auto"
};

const Container = {
  display: "flex",
  flexGrow: 1,
  flexDirection: "row",
  justifyContent: "space-evenly"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  flexWrap: "wrap"
};

const ContainerChild = {
  display: "flex",
  flexDirection: "row",
  maxHeight: "300px",
  marginRight: "80px",
  marginLeft: "80px"
};

class StatesInstance extends Component {
  state = {
    data: {}
  };

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(
          `https://d3aeapuik0gur3.cloudfront.net/org?org_name=${
            this.props.match.params.name
          }`
        )
      ])
      .then(
        axios.spread(orgResponse => {
          this.setState({
            data: orgResponse.data[0]
          });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  render() {
    var jsonStates = require("../data/states.json");
    const { data } = this.state;
    const { name, pics, mission, state, tagline, events, parks } =
      data !== undefined ? data : "";
    var parkStr = parks;
    var parkName;
    if (parkStr) {
      parkName = parkStr[0];
    }
    var eventsArr = events;
    var eventsName;
    if (eventsArr) {
      eventsName = eventsArr[0];
    }
    return (
      <React.Fragment>
        <div style={Background}>
          <div>
            <br />
            <h1 style={Header}>{name}</h1>
            <br />
          </div>
          <div style={Container}>
            <img alt="" style={ContainerChild} src={pics} />
          </div>
          <br />
          <OrgInfoCard
            style={ContainerChild}
            tagline={tagline}
            mission={mission}
            state={state}
          />
          <br />
          <div style={RelatedContainer}>
            {parkName !== undefined && parkName.length > 0 && (
              <RelatedCard
                title="Park"
                link={"/Parks/" + parkName}
                text={parkName}
              />
            )}
            {state !== undefined && state.length > 0 && (
              <RelatedCard
                title="States"
                link={"/States/" + jsonStates[state]}
                text={jsonStates[state]}
              />
            )}
            {events && (
              <RelatedCard
                title="Events"
                link={"/Events/" + eventsName}
                text={eventsName}
              />
            )}
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default StatesInstance;
