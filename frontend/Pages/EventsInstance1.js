import React, { Component } from "react";
import EventInfoCard from "../Components/EventInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";

const Header = {
  color: "black",
  fontSize: "50px",
  textAlign: "center"
};
const Background = {
  backgroundColor: "#ebfaeb"
};

const Container = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  flexWrap: "wrap"
};

// const InfoContainer = {
//   display: "flex",
//   flexGrow: 1
// }

class EventsInstance extends Component {
  render() {
    return (
      <React.Fragment>
        <div style={Background}>
          <div>
            <br />
            <h1 style={Header}>
              2019 Grand Canyon National Park 100th Birthday
            </h1>
            <br />
          </div>
          <div style={Container}>
            <img
              alt=""
              className="d-block h-50 w-50"
              src={
                "https://www.nps.gov/common/uploads/structured_data/3C7B1720-1DD8-B71B-0B74DCF6F887A960.jpg"
              }
            />
          </div>
          <div>
            <br />
            <EventInfoCard
              dates={"2019-02-26"}
              time={"09:00 AM - 09:00 PM"}
              location={"South Rim Village and Desert View"}
              park={"Grand Canyon National Park"}
              fee={"Free and open to the public"}
              description={
                "Grand Canyon’s Founders Day celebration will kick off with cake and a concert by the Flagstaff and Grand Canyon schools choir, cultural demonstrators, and cake on February 26 at the Historic Village, Visitor Center, and Desert View.\nStop by the visitor center to sign the park’s birthday card, share your favorite Grand Canyon memories at the oral history booth, and have some cake. Following the park’s birthday, special evening program speakers will talk about the importance relationship between Grand Canyon and the park’s 11 traditionally associated tribes.\nMore details and a schedule of Founder's Day events will be available here soon.\n"
              }
            />
            <br />
          </div>
          <div style={RelatedContainer}>
            <RelatedCard title="Parks" link="/Parks/3" text="Grand Canyon National Park"/>
            <RelatedCard title="States" link="/States/3" text="Arizona"/>
            <RelatedCard title="Organizations" link="/Orgs/1" text="Wildlife Conservation Network"/>
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default EventsInstance;
