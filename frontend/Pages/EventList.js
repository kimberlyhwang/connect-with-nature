import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListInfoCard from "../Components/ListInfoCard.js";
import Pagination from "../Components/Pagination.js";
import axios from "axios";

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: "inherit",
    width: "inherit"
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  container: {
    backgroundColor: "#ebfaeb",
    paddingTop: "3vh",
    minHeight: "100vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  },
  pagination: {}
});

function FormRow(props) {
  const data = props;
  if (data === undefined || data.data.length === 0) {
    return (
      <React.Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Events/1"
              coverName="Grand Canyon 100th Birthday"
              coverDescript="2019-02-26"
              coverImage="https://www.nps.gov/common/uploads/structured_data/3C7B1720-1DD8-B71B-0B74DCF6F887A960.jpg"
              attribute1="Dates"
              attribute1val="2019-02-27, 2019-02-28"
              attribute2="Time"
              attribute2val="04:00 PM - 04:30 PM"
              attribute3="Location"
              attribute3val="Grand Canyon Visitor Center"
              attribute4="National Park"
              attribute4val="Grand Canyon National Park"
              attribute5="Fee Information"
              attribute5val="Free and open to the public"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Events/1"
              coverName="Mammoth: Snowshoe Discovery"
              coverDescript="2019-02-27, 2019-03-01, 2019-03-02"
              coverImage="https://www.nps.gov/common/uploads/structured_data/3C7D34E6-1DD8-B71B-0BBB1C0F478318E2.jpg"
              attribute1="Dates"
              attribute1val="2019-02-27, 2019-03-01, 2019-03-02"
              attribute2="Time"
              attribute2val="02:00 PM - 04:00 PM"
              attribute3="Location"
              attribute3val="Meet at the Upper Terrace Drive entrance parking area two miles south of Mammoth. Be alert for vehicles driving along the road or in the parking lot."
              attribute4="National Park"
              attribute4val="Yellowstone National Park"
              attribute5="Fee Information"
              attribute5val="Snowshoes can be rented at the Bear Den ski shop inside the lobby of the Mammoth Hotel, or bring your own"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Events/1"
              coverName="Park Ranger Program: Critter Chat"
              coverDescript="2019-02-27, 2019-02-28"
              coverImage="https://www.nps.gov/common/uploads/structured_data/3C7B15A4-1DD8-B71B-0BFADECB506765CC.jpg"
              attribute1="Dates"
              attribute1val="2019-02-27, 2019-02-28"
              attribute2="Time"
              attribute2val=" 04:00 PM - 04:30 PM"
              attribute3="Location"
              attribute3val="Grand Canyon Visitor Center"
              attribute4="National Park"
              attribute4val="Grand Canyon National Park"
              attribute5="Fee Information"
              attribute5val="Free and open to the public"
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  } else {
    const length = data.data.length;
    return (
      <React.Fragment>
        <Grid container spacing={24}>
          {length > 0 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[0].name}
                coverName={data.data[0].name}
                coverDescript="potate"
                coverImage={data.data[0].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[0].date[0]}
                attribute2="Time"
                attribute2val={data.data[0].time}
                attribute3="Location"
                attribute3val={data.data[0].location}
                attribute4="National Park"
                attribute4val={data.data[0].park}
                attribute5="Fee Information"
                attribute5val={data.data[0].fee}
              />
            </Grid>
          )}
          {length > 1 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[1].name}
                coverName={data.data[1].name}
                coverDescript="potate"
                coverImage={data.data[1].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[1].date[0]}
                attribute2="Time"
                attribute2val={data.data[1].time}
                attribute3="Location"
                attribute3val={data.data[1].location}
                attribute4="National Park"
                attribute4val={data.data[1].park}
                attribute5="Fee Information"
                attribute5val={data.data[1].fee}
              />
            </Grid>
          )}
          {length > 2 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[2].name}
                coverName={data.data[2].name}
                coverDescript="potate"
                coverImage={data.data[2].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[2].date[0]}
                attribute2="Time"
                attribute2val={data.data[2].time}
                attribute3="Location"
                attribute3val={data.data[2].location}
                attribute4="National Park"
                attribute4val={data.data[2].park}
                attribute5="Fee Information"
                attribute5val={data.data[2].fee}
              />
            </Grid>
          )}
          {length > 3 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[3].name}
                coverName={data.data[3].name}
                coverDescript="potate"
                coverImage={data.data[3].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[3].date[0]}
                attribute2="Time"
                attribute2val={data.data[3].time}
                attribute3="Location"
                attribute3val={data.data[3].location}
                attribute4="National Park"
                attribute4val={data.data[3].park}
                attribute5="Fee Information"
                attribute5val={data.data[3].fee}
              />
            </Grid>
          )}
          {length > 4 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"/Events/" + data.data[4].name}
                coverName={data.data[4].name}
                coverDescript="potate"
                coverImage={data.data[4].pics[0]}
                attribute1="Dates"
                attribute1val={data.data[4].date[0]}
                attribute2="Time"
                attribute2val={data.data[4].time}
                attribute3="Location"
                attribute3val={data.data[4].location}
                attribute4="National Park"
                attribute4val={data.data[4].park}
                attribute5="Fee Information"
                attribute5val={data.data[4].fee}
              />
            </Grid>
          )}
        </Grid>
      </React.Fragment>
    );
  }
}

class EventList extends Component {
  state = {
    allEvents: [],
    currentEvents: [],
    currentPage: null,
    totalPages: null
  };

  scrapeProjectInfo() {
    axios.all([axios.get("https://api.connectwithnature.me/event")]).then(
      axios.spread(eventResponse => {
        this.setState({
          allEvents: eventResponse.data
        });
      })
    );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  onPageChanged = data => {
    const { allEvents } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentEvents = allEvents.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentEvents, totalPages });
  };

  render() {
    const { classes } = this.props;
    const { allEvents, currentEvents } = this.state;
    const totalEvents = allEvents.length;
    return (
      <div className = {classes.container}>
        <Grid container className={classes.root} spacing={8}>
          <Grid container item xs={12} spacing={16}>
            <FormRow data={currentEvents} />
          </Grid>
        </Grid>
 
        {totalEvents !== 0 && (
          <Pagination
            className={classes.pagination}
            totalRecords={totalEvents}
            pageLimit={5}
            onPageChanged={this.onPageChanged}
          />
        )}
      </div>
    );
  }
}

EventList.propTypes = {
  data: PropTypes.object
};

export default withStyles(styles)(EventList);
