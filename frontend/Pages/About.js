import React from "react";
import StatTable from "../Components/StatTable.js";
import AboutCard from "../Components/AboutCard.js";
import DeveloperCard from "../Components/DeveloperCard.js";
import allen from "../static/allen.png";
import deyuan from "../static/deyuan.jpeg";
import kimberly from "../static/kimberly.png";
import siwei from "../static/siwei.png";
import tin from "../static/tin.jpeg";
import { withStyles } from "@material-ui/core/styles";
import { Typography } from "@material-ui/core";

const styles = {
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    backgroundColor: "#ebfaeb",
    height: "auto"
  },
  containerDevs: {
    display: "flex",
    flexDirection: "rows",
    justifyContent: "space-evenly",
    flexWrap: "wrap",
  },
  header: {
    paddingTop: "24px",
    paddingBottom: "8px",
    color: "black",
    fontSize: "50px",
    textAlign: "center"
  },
  table: {
    margin: "1vh",
  }
};

function About(props) {
  const { classes } = props;
  return (
    <div className={classes.container}>
      <Typography className={classes.header}> About Us</Typography>
      <AboutCard
        title="Project Goal"
        text={
          "The goal of this project is to promote civic appreciation of nature and raise awareness for future conservation efforts by encouraging visitation to the national parks. We hope that this site will you help you better appreciate the importance of preservation and consider becoming involved in current conservation efforts!"
        }
      />
      <AboutCard
        title="Insight"
        text={
          "Conversation groups carry strong relations with national parks. Funding for various events and meetups are often funded by charities raising awareness. We hope to assist these organizations and bring public notice to their efforts."
        }
      />
      <div className={classes.containerDevs}>
        <DeveloperCard
          name="Siwei Mao"
          image={siwei}
          text={"Junior Computer Science major who likes eating\n \nResponsibilities: Front-end, Hosting"}
        />
        <DeveloperCard
          name="Deyuan Zhang"
          image={deyuan}
          text={"Junior Computer Science major who likes trading\n \nResponsibilities: Front-end, API Scraping"}
        />
        <DeveloperCard
          name="Tin La"
          image={tin}
          text={"Senior Computer Science major who likes exercising \n \n Responsibilities: Front-end, Git"}
        />
        <DeveloperCard
          name="Kimberly Hwang"
          image={kimberly}
          text={"Junior Computer Science major who likes corgis \n \n Responsibilities: Front-end, API Scraping"}
        />
        <DeveloperCard
          name="Allen Chen"
          image={allen}
          text={"Senior Computer Science major who likes running \n \n Responsibilities: Front-end, About Page "}
        />
      </div>
      <StatTable className={classes.table} />
      <AboutCard
        title="Data Sources"
        links={[
          "NPS API: https://www.nps.gov/subjects/developer/index.htm",
          "We used the NPS API to extract a JSON for information about different national parks. \n",
          "Wikipedia API: https://en.wikipedia.org/w/api.php",
          "We used the Wikipedia API to extract a JSON for information about the different states. \n",
          "Weather API: https://openweathermap.org",
          "We haven't used the Weather API yet, but we plan on using it in the future.",
          "CharityNavigator API: http://api.charitynavigator.org/",
          "We used the CharityNavigator API to extract a JSON for information about the different charities concerning nature conservation.",
          "GitLab API: https://gitlab.com",
          "We used the Gitlab API to dynamically update our commits and issues."
        ]}
      />
      <AboutCard
        title="Tools"
        links={[
          "Postman: https://www.getpostman.com/downloads/",
          "We used Postman to scrape data from APIs and create our Restful API.",
          "React: https://reactjs.org/",
          "We used React for the front-end framework for creating the user interface.",
          "NamesCheap: https://www.namecheap.com/",
          "We used NamesCheap to obtain a pretty URL.",
          "GitLab: https://gitlab.com/siweimao/connect-with-nature",
          "We used GitLab for version control.",
          "AWS: https://aws.amazon.com",
          "We used Amazon Web Services to host our website on the Internet."
        ]}
      />
      <AboutCard
        title="Links"
        links={[
          "GitLab Repository: https://gitlab.com/siweimao/connect-with-nature",
          "Postman: https://documenter.getpostman.com/view/6751555/S11LsHR5?fbclid=IwAR10TSdzgihDLtwcoBLRuJcoQhbrOONjXczM6E2nxHqpv3-yymc7seU7IbM#89ef26fb-38e5-48a0-b0b6-e8ba80d362c3"
        ]}
      />

    </div>
  );
}

export default withStyles(styles)(About);
