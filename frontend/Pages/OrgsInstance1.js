import React, { Component } from "react";
import OrgInfoCard from "../Components/OrgInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";

const Header = {
  color: "black",
  fontSize: "50px",
  textAlign: "center"
};
const Background = {
  backgroundColor: "#ebfaeb"
};

const Container = {
  display: "flex",
  flexGrow: 1,
  flexDirection: "row",
  justifyContent: "space-evenly"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  flexWrap: "wrap"
};

const ContainerChild = {
  display: "flex",
  flexDirection: "row",
  maxHeight: "300px",
  marginRight: "80px",
  marginLeft: "80px"
};

// const InfoContainer = {
//   display: "flex",
//   flexGrow: 1
// }

class StatesInstance extends Component {
  render() {
    return (
      <React.Fragment>
        <div style={Background}>
          <div>
            <br />
            <h1 style={Header}>Wildlife Conservation Network </h1>
            <br />
          </div>
          <div style={Container}>
            <img
              alt=""
              style={ContainerChild}
              src={
                "https://upload.wikimedia.org/wikipedia/en/0/06/Wildlife_Conservation_Network_logo.png"
              }
            />
            </div>
            <br/>
            <OrgInfoCard
              style={ContainerChild}
              tagline={"The spirit of innovation in conservation"}
              mission={
                "Founded in 2002, Wildlife Conservation Network (WCN) is dedicated to protecting endangered species and preserving their natural habitats. We support innovative strategies for people and wildlife to co-exist and thrive. We partner with independent, community-based conservationists around the world and provide them with the capital and tools they need to develop solutions for human-wildlife coexistence. WCN's conservationists actively engage local people as effective stewards of their environment and work in a culturally-respectful manner to ensure that conservation skills and values will be passed on to future generations."
              }
              state={"CA"}
            />
          <br />
          <div style={RelatedContainer}>
            <RelatedCard title="Parks" link="/Parks/2" text="Yosemite National Park"/>
            <RelatedCard title="States" link="/States/2" text="California"/>
            <RelatedCard title="Events" link="/Events/3" text="Park Ranger Program: Critter Chat"/>
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default StatesInstance;
