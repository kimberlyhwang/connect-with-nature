import React, { Component } from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import ListInfoCard from "../Components/ListInfoCard.js";
import Pagination from "../Components/Pagination.js";
import axios from "axios";

function FormRow(props) {
  const data = props;
  if (data === undefined || data.data.length === 0) {
    return (
      <React.Fragment>
        <Grid container spacing={24}>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Orgs/1"
              coverName="Wildlife Conservation Network"
              coverDescript="The spirit of innovation in conservation"
              coverImage="https://upload.wikimedia.org/wikipedia/en/0/06/Wildlife_Conservation_Network_logo.png"
              attribute1="Name"
              attribute1val="Wildlife Conservation Network"
              attribute2="Tagline"
              attribute2val="The spirit of innovation in conservation"
              attribute3="Category"
              attribute3val="Animals"
              attribute4="State"
              attribute4val="CA"
              attribute5="Rating"
              attribute5val="3"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Orgs/1"
              coverName="Santa Barbara Botanic Garden"
              coverDescript="Dedicated to research, education and conservation, and display of California native plants"
              coverImage="https://upload.wikimedia.org/wikipedia/commons/5/57/SBBotanicGarden1.JPG"
              attribute1="Name"
              attribute1val="Santa Barbara Botanic Garden"
              attribute2="Tagline"
              attribute2val="Dedicated to research, education and conservation, and display of California native plants"
              attribute3="Category"
              attribute3val="Environment"
              attribute4="State"
              attribute4val="CA"
              attribute5="Rating"
              attribute5val="4"
            />
          </Grid>
          <Grid item xs={12} sm={4}>
            <ListInfoCard
              link="Orgs/1"
              coverName="California Native Plant Society"
              coverDescript="Preserving and Protecting California Native Plants and Their Habitats"
              coverImage="https://upload.wikimedia.org/wikipedia/en/c/c9/CNPS.gif"
              attribute1="Name"
              attribute1val="California Native Plant Society"
              attribute2="Tagline"
              attribute2val="Preserving and Protecting California Native Plants and Their Habitats"
              attribute3="Category"
              attribute3val="Environment"
              attribute4="State"
              attribute4val="CA"
              attribute5="Rating"
              attribute5val="3"
            />
          </Grid>
        </Grid>
      </React.Fragment>
    );
  } else {
    const length = data.data.length;

    return (
      <React.Fragment>
        <Grid container spacing={24}>
          {length > 0 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[0].name}
                coverName={data.data[0].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[0].pics}
                attribute1="Name"
                attribute1val={data.data[0].name}
                attribute2="Tagline"
                attribute2val={data.data[0].tagline}
                attribute3="Category"
                attribute3val={data.data[0].category}
                attribute4="State"
                attribute4val={data.data[0].state}
                attribute5="Rating"
                attribute5val={data.data[0].rating}
              />
            </Grid>
          )}
          {length > 1 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[1].name}
                coverName={data.data[1].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[1].pics}
                attribute1="Name"
                attribute1val={data.data[1].name}
                attribute2="Tagline"
                attribute2val={data.data[1].tagline}
                attribute3="Category"
                attribute3val={data.data[1].category}
                attribute4="State"
                attribute4val={data.data[1].state}
                attribute5="Rating"
                attribute5val={data.data[1].rating}
              />
            </Grid>
          )}
          {length > 2 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[2].name}
                coverName={data.data[2].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[2].pics}
                attribute1="Name"
                attribute1val={data.data[2].name}
                attribute2="Tagline"
                attribute2val={data.data[2].tagline}
                attribute3="Category"
                attribute3val={data.data[2].category}
                attribute4="State"
                attribute4val={data.data[2].state}
                attribute5="Rating"
                attribute5val={data.data[2].rating}
              />
            </Grid>
          )}
          {length > 3 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[3].name}
                coverName={data.data[3].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[3].pics}
                attribute1="Name"
                attribute1val={data.data[3].name}
                attribute2="Tagline"
                attribute2val={data.data[3].tagline}
                attribute3="Category"
                attribute3val={data.data[3].category}
                attribute4="State"
                attribute4val={data.data[3].state}
                attribute5="Rating"
                attribute5val={data.data[3].rating}
              />
            </Grid>
          )}
          {length > 4 && (
            <Grid item xs={12} sm={4}>
              <ListInfoCard
                link={"Orgs/" + data.data[4].name}
                coverName={data.data[4].name}
                coverDescript="Dedicated to research, education and conservation, and display of California native plants"
                coverImage={data.data[4].pics}
                attribute1="Name"
                attribute1val={data.data[4].name}
                attribute2="Tagline"
                attribute2val={data.data[4].tagline}
                attribute3="Category"
                attribute3val={data.data[4].category}
                attribute4="State"
                attribute4val={data.data[4].state}
                attribute5="Rating"
                attribute5val={data.data[4].rating}
              />
            </Grid>
          )}
        </Grid>
      </React.Fragment>
    );
  }
}

const styles = theme => ({
  root: {
    flexGrow: 1,
    height: "inherit",
    width: "inherit"
  },
  paper: {
    padding: theme.spacing.unit,
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  container: {
    backgroundColor: "#ebfaeb",
    paddingTop: "3vh",
    minHeight: "100vh",
    height: "auto",
    display: "flex",
    flexDirection: "column",
    alignItems: "center"
  }
});

class OrgList extends Component {
  state = {
    allOrgs: [],
    currentOrgs: [],
    currentPage: null,
    totalPages: null
  };

  scrapeProjectInfo() {
    axios.all([axios.get("https://api.connectwithnature.me/org")]).then(
      axios.spread(orgResponse => {
        this.setState({
          allOrgs: orgResponse.data
        });
      })
    );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  onPageChanged = data => {
    const { allOrgs } = this.state;
    const { currentPage, totalPages, pageLimit } = data;

    const offset = (currentPage - 1) * pageLimit;
    const currentOrgs = allOrgs.slice(offset, offset + pageLimit);

    this.setState({ currentPage, currentOrgs, totalPages });
  };

  render() {
    const { classes } = this.props;
    // const { allOrgs, currentOrgs, currentPage, totalPages } = this.state;
    const { allOrgs, currentOrgs } = this.state;
    const totalOrgs = allOrgs.length;
    return (
      <div className={classes.container}>
        <Grid container className={classes.root} spacing={8}>
          <Grid container item xs={12} spacing={16}>
            <FormRow data={currentOrgs} />
          </Grid>
        </Grid>

        {totalOrgs !== 0 && (
          <Pagination
            className={classes.pagination}
            totalRecords={totalOrgs}
            pageLimit={5}
            onPageChanged={this.onPageChanged}
          />
        )}
      </div>
    );
  }
}

OrgList.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(OrgList);
