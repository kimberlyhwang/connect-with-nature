import React, { Component } from "react";
import EventInfoCard from "../Components/EventInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";
import axios from "axios";
import patrick from "../Components/hmm.gif";

const Header = {
  color: "black",
  fontSize: "50px",
  textAlign: "center"
};
const Background = {
  backgroundColor: "#ebfaeb"
};

const Container = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  flexWrap: "wrap"
};

class EventsInstance extends Component {
  state = {
    data: {
      pics: [patrick]
    }
  };

  scrapeProjectInfo() {
    axios
      .all([
        axios.get(
          `https://d3aeapuik0gur3.cloudfront.net/event?event_name=${
            this.props.match.params.name
          }`
        )
      ])
      .then(
        axios.spread(eventResponse => {
          console.log(eventResponse.data);
          this.setState({
            data: eventResponse.data[0]
          });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }
  render() {
    var jsonStates = require("../data/states.json");
    const { data } = this.state;
    const {
      name,
      date,
      fee,
      location,
      park,
      pics,
      summary,
      time,
      states,
      orgs
    } = data;
    var orgsArr = orgs;
    var orgName;
    if (orgsArr) {
      orgName = orgsArr[0];
    }
    var str = states;
    var res;
    if (states) {
      res = str.split(",");
      str = res[0];
    }
    return (
      <React.Fragment>
        <div style={Background}>
          <div>
            <br />
            <h1 style={Header}>{name}</h1>
            <br />
          </div>
          <div style={Container}>
            <img alt="" className="d-block h-50 w-50" src={pics[0]} />
          </div>
          <div>
            <br />
            <EventInfoCard
              dates={date}
              time={time}
              location={location}
              park={park}
              fee={fee}
              description={summary}
            />
            <br />
          </div>
          <div style={RelatedContainer}>
            {park !== undefined && park.length > 0 && (
              <RelatedCard title="Park" link={"/Parks/" + park} text={park} />
            )}

            {states !== undefined && states.length > 0 && (
              <RelatedCard
                title="States"
                link={"/States/" + jsonStates[str]}
                text={jsonStates[str]}
              />
            )}

            {orgs && (
              <RelatedCard
                title="Organization"
                link={"/Orgs/" + orgName}
                text={orgName}
              />
            )}
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default EventsInstance;
