import React, { Component } from "react";
import ParkInfoCard from "../Components/ParkInfoCard.js";
import RelatedCard from "../Components/RelatedCard.js";

const Header = {
  color: "black",
  fontSize: "50px",
  textAlign: "center"
};
const Background = {
  backgroundColor: "#ebfaeb"
};

const Container = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly"
};

const RelatedContainer = {
  display: "flex",
  flexDirection: "row",
  justifyContent: "space-evenly",
  flexWrap: "wrap"
};


class ParksInstance extends Component {
  render() {
    return (
      <React.Fragment>
        <div style={Background}>
          <div>
            <br />
            <h1 style={Header}>Yellowstone National Park</h1>
            <br />
          </div>
          <div style={Container}>
            <img
              alt=""
              className="d-block h-50 w-50"
              src={
                "https://www.nps.gov/common/uploads/structured_data/3C7D368B-1DD8-B71B-0B92925065B93463.jpg"
              }
            />
          </div>
          <div>
            <br />
            <ParkInfoCard
              states={"ID, MT, WY"}
              summary={
                "Visit Yellowstone and experience the world's first national park. Marvel at a volcano’s hidden power rising up in colorful hot springs, mudpots, and geysers. Explore mountains, forests, and lakes to watch wildlife and witness the drama of the natural world unfold. Discover the history that led to the conservation of our national treasures “for the benefit and enjoyment of the people."
              }
              directions={
                "Yellowstone National Park covers nearly 3,500 square miles in the northwest corner of Wyoming (3% of the park is in Montana and 1% is in Idaho). Yellowstone has five entrance stations, and several are closed to regular vehicles during winter. It takes many hours to drive between these entrances, so be sure to check the status of roads at the entrance you intend to use while planning your trip and before you arrive."
              }
              weather={
                "Yellowstone's weather can vary quite a bit, even in a single day. In the summer, daytime highs can exceed 70F (25C), only to drop 20 or more degrees when a thunderstorm rolls through. It can snow during any month of the year, and winter lows frequently drop below zero, especially at night. Bring a range of clothing options, including a warm jacket and rain gear, even in the summer."
              }
            />
            <br />
          </div>
          <div style={RelatedContainer}>
            <RelatedCard title="States" link="/States/1" text="Idaho"/>
            <RelatedCard title="Events" link="/Events/2" text="Mammoth: Snowshoe Discovery"/>
            <RelatedCard title="Organizations" link="/Orgs/1" text="Wildlife Conservation Network"/>
          </div>
          <br />
        </div>
      </React.Fragment>
    );
  }
}

export default ParksInstance;
