import React from "react";
import Collage from "../Components/Collage";
import FrontCard from "../Components/FrontCard";
import { withStyles } from "@material-ui/core/styles";
import S from "../Components/S.png";
import P from "../Components/P.png";
import O from "../Components/O.png";
import E from "../Components/E.png";
import { Link } from "react-router-dom";

const styles = {
  container: {
    display: "flex",
    flexDirection: "rows",
    flexFlow: "row wrap",
    justifyContent: "center",
    backgroundColor: "#ebfaeb",
    padding: "3vh"
  }
};

function Home(props) {
  const { classes } = props;
  return (
    <React.Fragment>
      <Collage className />
      <div className={classes.container}>
        <Link to="/States">
          <FrontCard title="States" img={S} />
        </Link>
        <Link to="/Parks">
          <FrontCard title="Parks" img={P} />
        </Link>
        <Link to="/Orgs">
          <FrontCard title="Organizations" img={O} />
        </Link>
        <Link to="/Events">
          <FrontCard title="Events" img={E} />
        </Link>
      </div>
    </React.Fragment>
  );
}

export default withStyles(styles)(Home);
