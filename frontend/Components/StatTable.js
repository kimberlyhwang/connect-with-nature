import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { apiKey, projID } from "../Pages/config.js";
import axios from "axios";

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
    textAlign: 'center',
    fontSize: 18
  },
  body: {
    fontSize: 15,
    textAlign: 'center'
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: "45vw",
    overflowX: "auto",
    margin: "1vh"
  },
  table: {
    width: '45vw'
  }
});

class StatTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gitStats: {}
    };
    this.scrapeProjectInfo = this.scrapeProjectInfo.bind(this);
  }

  scrapeProjectInfo() {
    axios
      .all([
        axios.get("https://gitlab.com/api/v4/projects/" + projID + "/members", {
          params: {
            private_token: apiKey
          }
        }),
        axios.get(
          "https://gitlab.com/api/v4/projects/" +
            projID +
            "/repository/contributors",
          {
            params: {
              private_token: apiKey
            }
          }
        ),
        axios.get("https://gitlab.com/api/v4/projects/" + projID + "/issues", {
          params: {
            state: 'all',
            per_page: '100'
          }
        })
      ])
      .then(
        axios.spread((firstResponse, secondResponse, thirdResponse) => {
          var tempDict = {};
          var totalCommits = 0;
          var totalIssues = 0;
          firstResponse.data.map(x => {
            tempDict[x.name] = { issueCount: 0, commitCount: 0 };
            return 0;
          });

          secondResponse.data.map(x => {
            if (x.name === "awchen217") {
              tempDict["Allen Chen"].commitCount += x.commits;
            } else {
              tempDict[x.name].commitCount += x.commits;
            }
            totalCommits += x.commits;
            return 0;
          });
          
          thirdResponse.data.map(x => {
            if (x.author.name in tempDict) {
                tempDict[x.author.name].issueCount += 1;
                totalIssues += 1;
            }
            return 0;
          });
          tempDict["Total"] = {
            issueCount: totalIssues,
            commitCount: totalCommits
          };

          this.setState({ gitStats: tempDict });
        })
      );
  }

  componentDidMount() {
    this.scrapeProjectInfo();
  }

  createData(name, commits, issues, unitTests) {
    return { name, commits, issues, unitTests };
  }

  render() {
    var id = 0;
    const { classes } = this.props;
    return (
      <Paper className={classes.root}>
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <CustomTableCell>Name</CustomTableCell>
              <CustomTableCell align="right">Commits</CustomTableCell>
              <CustomTableCell align="right">Issues</CustomTableCell>
              <CustomTableCell align="right">Unit Tests</CustomTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {Object.keys(this.state.gitStats).map(key => (
              <TableRow key={id++}>
                <CustomTableCell component="th" scope="row">
                  {key}
                </CustomTableCell>
                <CustomTableCell align="right">
                  {this.state.gitStats[key].commitCount}
                </CustomTableCell>
                <CustomTableCell align="right">
                  {this.state.gitStats[key].issueCount}
                </CustomTableCell>
                <CustomTableCell align="right">0</CustomTableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Paper>
    );
  }
}

export default withStyles(styles)(StatTable);
