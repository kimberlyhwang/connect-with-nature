import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import { Link } from 'react-router-dom';


const styles = {
  card: {
    maxWidth: "345"
  },
  media: {
    objectFit: "contain"
  },
  text: {
    display: "flex",
    justifyContent: "center"
  }
};

function ListInfoCard(props) {
  const { classes } = props;
  return (
    <Card className={classes.root}>
      <Link to={props.link}>
        <CardActionArea>
          <CardMedia
            component="img"
            alt={props.coverDescript}
            className={classes.media}
            height="345"
            image={props.coverImage}
            title={props.coverName}
            text={props.text}
          />
          <CardContent>
            <Typography
              gutterBottom
              variant="h5"
              component="h2"
              className={classes.text}>
              {props.coverName}
            </Typography>

            <Typography>
            <b>{props.attribute1}:</b> {props.attribute1val}
            <br />
            <b>{props.attribute2}:</b> {props.attribute2val}
            <br />
            <b>{props.attribute3}:</b> {props.attribute3val}
            <br />
            <b>{props.attribute4}:</b> {props.attribute4val}
            <br />
            <b>{props.attribute5}:</b> {props.attribute5val}
            </Typography>
            {/* <Typography component="p">
              Lizards are a widespread group of squamate reptiles, with over 6,000
              species, ranging across all continents except Antarctica
            </Typography> */}
          </CardContent>
        </CardActionArea>
      </Link>
    </Card>
  );
}

ListInfoCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ListInfoCard);
