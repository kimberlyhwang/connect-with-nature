import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import { Link } from 'react-router-dom';
import CardActionArea from "@material-ui/core/CardActionArea";

const styles = {
  card: {
    minWidth: 275,
    display: "flex"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
};

function RelatedCard(props) {
  const { classes } = props;

  return (

    <Link to={props.link}>
      <CardActionArea>
        <Card className={classes.card}>
          <CardContent>
            <Typography variant="body2"><b>Related {props.title}:</b></Typography>
            <Typography variant="body2">{props.text}</Typography>
          </CardContent>
        </Card>
      </CardActionArea>
    </Link>

  );
}

RelatedCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(RelatedCard);
