import React, { Component } from "react";
import { Carousel } from "react-bootstrap";
import test1 from "./one.jpg";
import test2 from "./two.jpg";
import test3 from "./three.jpg";

class Collage extends Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      index: 0,
      direction: null
    };

    this.handleSelect = this.handleSelect.bind(this);
  }

  handleSelect(selectedIndex, e) {
    this.setState({
      index: selectedIndex,
      direction: e.direction
    });
  }

  render() {
    const { index, direction } = this.state;

    return (
      <Carousel
        activeIndex={index}
        direction={direction}
        onSelect={this.handleSelect}
      >
        <Carousel.Item>
          <img className="img-fluid" src={test1} alt="" />
          <Carousel.Caption>
            <h3>Discover the beauty of conserved nature in the U.S!</h3>
            <p>There are over 60 National parks in the United States.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="img-fluid" src={test2} alt="" />
          <Carousel.Caption>
            <h3>Read about nature conservation organizations!</h3>
            <p>Support a great cause.</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img className="img-fluid" src={test3} alt="" />
          <Carousel.Caption>
            <h3>Find events in National Parks that interest you!</h3>
            <p>Connect with others and revel in nature.</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    );
  }
}

export default Collage;
