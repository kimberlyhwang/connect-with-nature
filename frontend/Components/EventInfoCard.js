import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const styles = {
  card: {
    marginRight: "80px",
    marginLeft: "80px"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
};

function EventInfoCard(props) {
  const { classes } = props;

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography variant="body1">
          <b>Dates:</b> {props.dates}
        </Typography>
        <Typography variant="body1">
          <br />
          <b>Time:</b> {props.time}
        </Typography>
        <Typography variant="body1">
          <br />
          <b>Location:</b> {props.location}
        </Typography>
        <Typography variant="body1">
          <br />
          <b>National Park:</b> {props.park}
        </Typography>
        <Typography variant="body1">
          <br />
          <b>Fee Information:</b> {props.fee}
        </Typography>
        <Typography variant="body1">
          <br />
          <b>Description:</b> {props.description}
        </Typography>
      </CardContent>
    </Card>
  );
}

EventInfoCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(EventInfoCard);
