import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const styles = {
  card: {
    marginRight: "80px",
    marginLeft: "80px"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
};

function StateInfoCard(props) {
  const { classes } = props;

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography variant="body2">
          <b>Summary: </b>
          {props.summary}
        </Typography>
      </CardContent>
    </Card>
  );
}

StateInfoCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(StateInfoCard);
