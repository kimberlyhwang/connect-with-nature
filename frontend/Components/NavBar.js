import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import { Navbar, Nav } from "react-bootstrap";

const styles = {
  root: {
    backgroundColor: "#014421"
  },
  button: {
    color: "white"
  }
};

function NavBar(props) {
  return (
    <Navbar style={styles.root} expand="lg">
      <Navbar.Brand style={styles.button} href="/Home">
        Connect With Nature
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="navbar-nav ml-auto">
          <Nav.Link style={styles.button} href="/Parks">
            Parks
          </Nav.Link>
          <Nav.Link style={styles.button} href="/States">
            States
          </Nav.Link>
          <Nav.Link style={styles.button} href="/Events">
            Events
          </Nav.Link>
          <Nav.Link style={styles.button} href="/Orgs">
            Organizations
          </Nav.Link>
          <Nav.Link style={styles.button} href="/About">
            About
          </Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}

NavBar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(NavBar);
