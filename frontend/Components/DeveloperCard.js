import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const styles = {
  cardStyle: {
    minWidth: 250,
    minHeight: 300,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "10vh",
    margin: "10px"
  },
  title: {
    textAlign: "center",
    padding: "12px"
  },
  body: {
    textAlign: "center",
    fontSize: "16px",
    whiteSpace: 'pre-line'
  },
  image: {
    maxWidth: "100%",
    height: "auto"
  }
};

function DeveloperCard(props) {
  const { classes, name, text, image } = props;

  return (
    <Card className={classes.cardStyle}>
      <CardContent>
        <Typography className={classes.title} variant="title">
          {name}
        </Typography>
        <img alt="" className={classes.image} src={image} />
        <Typography className={classes.body} variant="body2">
          {text}
        </Typography>
      </CardContent>
    </Card>
  );
}

DeveloperCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(DeveloperCard);
