import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";

const styles = {
  card: {
    marginRight: "80px",
    marginLeft: "80px"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
};

function ParkInfoCard(props) {
  const { classes } = props;

  return (
    <Card className={classes.card}>
      <CardContent>
        <Typography variant="body1">
          <b>States:</b> {props.states}
        </Typography>
        <Typography variant="body1">
          <br />
          <b>Summary:</b> {props.summary}
        </Typography>
        <Typography variant="body1">
          <br />
          <b>Directions:</b> {props.directions}
        </Typography>
        <Typography variant="body1">
          <br />
          <b>Weather:</b> {props.weather}
        </Typography>
      </CardContent>
    </Card>
  );
}

ParkInfoCard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(styles)(ParkInfoCard);
