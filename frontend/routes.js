import React from "react";
import Navbar from "./Components/NavBar.js";
import Home from "./Pages/Home.js";
import ParksInstance from "./Pages/ParkInstance.js";
import ParksInstance1 from "./Pages/ParksInstance1.js";
import StatesInstance from "./Pages/StateInstance.js";
import StatesInstance1 from "./Pages/StatesInstance1.js";
import OrgsInstance from "./Pages/OrgInstance.js";
import OrgsInstance1 from "./Pages/OrgsInstance1.js";
import EventsInstance from "./Pages/EventInstance.js";
import EventsInstance1 from "./Pages/EventsInstance1.js";
import ParkList from "./Pages/ParkList.js";
import StateList from "./Pages/StateList.js";
import OrgList from "./Pages/OrgList.js";
import EventList from "./Pages/EventList.js";
import PlaceHolder from "./Pages/PlaceHolder.js";
import About from "./Pages/About.js";
import { Route, Switch, Redirect } from "react-router-dom";

export const Routes = () => {
  return (
    <div>
      <Navbar />
      <Switch>
        <Route exact path="/Home" component={Home} />
        <Route exact path="/">
          <Redirect to="/Home" />
        </Route>

        <Route exact path="/Parks" component={ParkList} />
        <Route path="/Parks/1" component={ParksInstance1} />
        <Route path="/Parks/:name" component={ParksInstance} />

        <Route exact path="/States" component={StateList} />
        <Route path="/States/1" component={StatesInstance1} />
        <Route path="/States/:name" component={StatesInstance} />

        <Route exact path="/Orgs" component={OrgList} />
        <Route path="/Orgs/1" component={OrgsInstance1} />
        <Route path="/Orgs/:name" component={OrgsInstance} />

        <Route exact path="/Events" component={EventList} />
        <Route path="/Events/1" component={EventsInstance1} />
        <Route path="/Events/:name" component={EventsInstance} />

        <Route exact path="/About" component={About} />
        <Route component={PlaceHolder} />
      </Switch>
    </div>
  );
};
