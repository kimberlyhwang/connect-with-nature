from unittest import main, TestCase
from selenium import webdriver

class UnitTests (TestCase) :
    @classmethod
    def setUpClass(self):
        self.driver = webdriver.Chrome(r'C:\Users\Allen\Downloads\Program Downloads\chromedriver_win32\chromedriver.exe')
        self.driver.implicitly_wait(30)
        self.driver.maximize_window()
        self.driver.get("https://www.connectwithnature.me")
        self.driver.title
    
    def test_navbar_park_link(self):
        park_link = self.driver.find_element_by_xpath('//a[@href="/Parks"]')
        park_link.click()
        anchorTags = self.driver.find_elements_by_tag_name("a")
        pageContainsNationalPark = False
        for anchorTag in anchorTags:
            elementToTest = self.driver.find_element_by_link_text(anchorTag.text)
            if "Park" in elementToTest.text:
                pageContainsNationalPark = True
                break
                
        assert pageContainsNationalPark
        assert "Park" in self.driver.current_url
    
    def test_navbar_state_link(self):
        state_link = self.driver.find_element_by_xpath('//a[@href="/States"]')
        state_link.click()
        assert "State" in self.driver.current_url
    
    def test_navbar_event_link(self):
        org_link = self.driver.find_element_by_xpath('//a[@href="/Events"]')
        org_link.click()
        assert "Events" in self.driver.current_url
        
    def test_navbar_org_link(self):
        org_link = self.driver.find_element_by_xpath('//a[@href="/Orgs"]')
        org_link.click()
        assert "Orgs" in self.driver.current_url
        
    def test_navbar_about_link(self):
        org_link = self.driver.find_element_by_xpath('//a[@href="/About"]')
        org_link.click()
        assert "About" in self.driver.current_url
        
    def test_navbar_home_link(self):
        org_link = self.driver.find_element_by_xpath('//a[@href="/Home"]')
        org_link.click()
        assert "Home" in self.driver.current_url
        
    def test_navbar_inavlid_link(self):
        bad_url = "https://www.connectwithnature.me/invalid"
        self.driver.get(bad_url)
        invalidImage = self.driver.find_element_by_tag_name("img")
        assert invalidImage.get_attribute("alt") == "thinking..."
        
    # def test_state_pagination(self):
         # self.pagination_test('https://www.connectwithnature.me/States')
    
    def test_org_pagination(self):
        self.pagination_test('https://www.connectwithnature.me/Orgs')
    
    def test_event_pagination(self):
        self.pagination_test('https://www.connectwithnature.me/Events')
    
    def test_park_pagination(self):
        self.pagination_test('https://www.connectwithnature.me/Parks')
                
    def pagination_test(self, url):
        self.driver.get(url)
        imgTags = self.driver.find_elements_by_tag_name("img")
        
        # parks on the first page
        orig_models = []
        for img in imgTags:
            model_instance = img.get_attribute("title")
            orig_models.append(model_instance)

        page = self.driver.find_element_by_class_name("pagination")
        items = page.find_elements_by_tag_name("li")
        for item in items:
            # check start on the first page
            if item.get_attribute('class') == 'page-item active':
                assert item.find_element_by_tag_name('a').get_attribute('innerHTML') == '1'
            # navigate to the second page
            if  item.find_element_by_tag_name('a').get_attribute('aria-label') == 'Next':
                item.click()
                break
                
        # events on the second page
        newImgTags = self.driver.find_elements_by_tag_name("img")
        for img in newImgTags:
            model_instance = img.get_attribute("title")
            assert model_instance not in orig_models
            
        page = self.driver.find_element_by_class_name("pagination")
        items = page.find_elements_by_tag_name("li")
        for item in items:
            # check on second page
            if item.get_attribute('class') == 'page-item active':
                assert item.find_element_by_tag_name('a').get_attribute('innerHTML') == '2'
                break

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

if __name__ == "__main__" :
    main()
